package rgo;

import java.awt.*;
import javax.swing.*;
import static java.awt.MouseInfo.getPointerInfo;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class RGOEngine extends JPanel {

    public final static int WIDTH = 1280;
    public final static int HEIGHT = 720;
    private static boolean running = true;
    private static boolean paused = false;
    private int fps = 60;
    private int frameCount = 0;

    JFrame frame;

    static Graphics buffer;
    static Image offscreen;
    RGOEngine rgo;

    int cursorX, cursorY, lastCursorX, lastCursorY;

    public RGOEngine() {
        offscreen = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        buffer = offscreen.getGraphics();
        setFocusable(true);

        cursorX = lastCursorX = 50;
        cursorY = lastCursorY = 50;

        frame = new JFrame("RPG Gale Online");
        frame.add(this);
        frame.setSize(WIDTH, HEIGHT);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        runGameLoop();
    }

    public static void clearBuffer() {
        Color originalColor = buffer.getColor();
        buffer.setColor(Color.black);
        buffer.fillRect(0, 0, WIDTH, HEIGHT);
        buffer.setColor(originalColor);
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(offscreen, 0, 0, this);
    }

    public static void main(String[] args) throws InterruptedException {
        RGOEngine rgo = new RGOEngine();
    }

    public void runGameLoop() {
        Thread loop = new Thread() {
            public void run() {
                gameLoop();
            }
        };
        loop.start();
    }

    //only run in another thread
    private void gameLoop() {
        final double GAME_HERTZ = 25.0;
        final double TIME_BETWEEN_UPDATES = 1000000000 / GAME_HERTZ;
        final int MAX_UPDATES_BEFORE_RENDER = 5;
        double lastUpdateTime = System.nanoTime();
        double lastRenderTime = System.nanoTime();

        final double TARGET_FPS = 255; //maximum fps
        final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;

        int lastSecondTime = (int) (lastUpdateTime / 1000000000);

        while (running) {
            double now = System.nanoTime();
            int updateCount = 0;

            if (!paused) {
                //Update the game 25times per second
                while (now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER) {
                    updateGame();
                    lastUpdateTime += TIME_BETWEEN_UPDATES;
                    updateCount++;
                }
                //In the case an update take too long
                if (now - lastUpdateTime > TIME_BETWEEN_UPDATES) {
                    lastUpdateTime = now - TIME_BETWEEN_UPDATES;
                }

                //Render with interpolation
                float interpolation = Math.min(1.0f, (float) ((now - lastUpdateTime) / TIME_BETWEEN_UPDATES));
                drawGame(interpolation);
                lastRenderTime = now;

                int thisSecond = (int) (lastUpdateTime / 1000000000);
                if (thisSecond > lastSecondTime) {
                    System.out.println("NEW SECOND " + thisSecond + " " + frameCount);
                    fps = frameCount;
                    frameCount = 0;
                    lastSecondTime = thisSecond;
                }

                //Save cpu
                while (now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS && now - lastUpdateTime < TIME_BETWEEN_UPDATES) {
                    Thread.yield();

                    //This stops the app from consuming all your CPU. It makes this slightly less accurate, but is worth it.
                    //You can remove this line and it will still work (better), your CPU just climbs on certain OSes.
                    //FYI on some OS's this can cause pretty bad stuttering. Scroll down and have a look at different peoples' solutions to this.
                    try {
                        Thread.sleep(1);
                    } catch (Exception e) {
                    }

                    now = System.nanoTime();
                }
            }
        }
    }

    private void updateGame() {
        lastCursorX = cursorX;
        lastCursorY = cursorY;

        cursorX = getPointerInfo().getLocation().x - getLocationOnScreen().x;
        cursorY = getPointerInfo().getLocation().y - getLocationOnScreen().y;
        //getPointerInfo().getLocation().x - rgo.getLocationOnScreen().x
    }

    private void drawGame(float interpolation) {
        clearBuffer();
        buffer.drawString("FPS: " + fps, 5, 10);
        buffer.drawString("Interpolation: " + (int) (interpolation * 100) , 5, 25);
        
        buffer.drawString("Hello World ! (hehe)", (int) ((cursorX - lastCursorX) * interpolation + lastCursorX), (int) ((cursorY - lastCursorY) * interpolation + lastCursorY));
        //paint(buffer);
        repaint();
        //rgo.repaint();
        frameCount++;
    }
}
