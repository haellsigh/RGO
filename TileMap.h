#ifndef TILEMAP_H
#define TILEMAP_H

#include <SFML/Graphics.hpp>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include "SimpleIni.h"

class TileMap : public sf::Drawable, public sf::Transformable
{
    public:
        TileMap();
        bool load(const std::string& tileset, sf::Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height);

    private:
        std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
        std::vector<std::string> split(const std::string &s, char delim);
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        sf::VertexArray m_vertices;
        sf::Texture m_tileset;
        CSimpleIniA *ini;
};

#endif // TILEMAP_H
